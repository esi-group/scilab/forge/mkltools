/* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2010 */
/* ========================================================================== */
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"
#include "mkl_wrap.h"
/* ========================================================================== */
int sci_MKL_Installed(char *fname)
{
    Rhs = max(Rhs, 0);

    CheckRhs(0, 0);
    CheckLhs(1, 1);

    createScalarBoolean(pvApiCtx, Rhs + 1, MKL_Installed_wrap());
    LhsVar(1) = Rhs + 1;
    C2F(putlhsvar)();
    return 0;
}
/* ========================================================================== */
