/* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2010 */
/* ========================================================================== */
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"
#include "mkl_wrap.h"
/* ========================================================================== */
int sci_MKL_Domain_Set_Num_Threads(char *fname)
{
    SciErr sciErr;

    int *piAddressVarOne = NULL;
    int *piAddressVarTwo = NULL;

    int iErr = 0;
    int iNum = 0;
    int iMask = 0;

    double dNum = 0;
    double dMask = 0;


    CheckRhs(2, 2);
    CheckLhs(1, 1);

    if (!MKL_Installed_wrap())
    {
        Scierror(999,_("%s: MKL not detected.\n"), fname);
        return 0;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (isDoubleType(pvApiCtx, piAddressVarOne))
    {
        double dValue = 0.;

        if (!isScalar(pvApiCtx, piAddressVarOne))
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"), fname, 1);
            return 0;
        }

        getScalarDouble(pvApiCtx, piAddressVarOne, &dValue);
        iNum = (int)dValue;

        if ((double)iNum != dValue)
        {
            Scierror(999,_("%s: Wrong value for input argument #%d: A integer value expected.\n"), fname, 1);
            return 0;
        }
    }
    else
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: An integer value expected.\n"), fname, 1);
        return 0;
    }

    if (isDoubleType(pvApiCtx, piAddressVarTwo))
    {
        double dValue = 0.;

        if (!isScalar(pvApiCtx, piAddressVarTwo))
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"), fname, 2);
            return 0;
        }

        getScalarDouble(pvApiCtx, piAddressVarTwo, &dValue);
        iMask = (int)dValue;

        if ((double)iMask != dValue)
        {
            Scierror(999,_("%s: Wrong value for input argument #%d: A integer value expected.\n"), fname, 2);
            return 0;
        }

        if ((iMask < MKL_ALL) && (iMask > MKL_VML))
        {
            Scierror(999,_("%s: Wrong value for input argument #%d: 0,1,2 or 3 expected.\n"), fname, 2);
            return 0;
        }
    }
    else
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: An integer value expected.\n"), fname, 2);
        return 0;
    }


    iErr = MKL_Domain_Set_Num_Threads_wrap(iNum, iMask);
    createScalarBoolean(pvApiCtx, Rhs + 1, iErr);
    LhsVar(1) = Rhs + 1;

    C2F(putlhsvar)();

    return 0;
}
/* ========================================================================== */
