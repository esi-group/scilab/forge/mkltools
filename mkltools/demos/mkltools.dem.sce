// Copyright (C) 2010 - DIGITEO - Allan CORNET

function demo_mkltools()

  if MKL_Installed() then
    disp("mkltools demo");
    disp("MKL version:");
    disp(MKL_Get_Version_String());
    disp("CPU frequency (Ghz):");
    disp(MKL_Get_Cpu_Frequency());
    disp("Numbers of Threads:");
    disp(MKL_Get_Max_Threads());
  end

endfunction


demo_mkltools();
clear demo_mkltools;
